import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { GroupsService } from './groups.service';
import { GroupsController } from './groups.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Group } from './entities/group.entity';
import { CheckGroupExistsMiddleware } from './middlewares/group.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([Group])],
  providers: [GroupsService],
  controllers: [GroupsController],
  exports: [GroupsService],
})
export class GroupsModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(CheckGroupExistsMiddleware)
      .forRoutes(
        { path: 'groups/:id', method: RequestMethod.PATCH },
        { path: 'groups/:id', method: RequestMethod.DELETE },
      );
  }
}
