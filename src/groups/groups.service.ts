import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Group } from './entities/group.entity';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { IGroupWithStudents } from './interfaces/group-with-students.interface';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private readonly groupsRepository: Repository<Group>,
  ) {}

  async findGroupById(groupId: number): Promise<Group | null> {
    const group = await this.groupsRepository.findOne({
      where: { id: groupId },
    });
    return group;
  }

  async getAllGroups(): Promise<Group[]> {
    return this.groupsRepository.find({});
  }

  async getGroupWithStudentsById(id: number): Promise<IGroupWithStudents> {
    const group = await this.groupsRepository
      .createQueryBuilder('group')
      .leftJoinAndSelect('group.students', 'student')
      .where('group.id = :id', { id })
      .getOne();

    if (!group) {
      throw new NotFoundException('Group not found');
    }

    return group;
  }

  async createGroup(createGroupDto: CreateGroupDto): Promise<Group> {
    return this.groupsRepository.save(createGroupDto);
  }

  async updateGroupById(
    id: number,
    updateGroupDto: UpdateGroupDto,
  ): Promise<UpdateResult> {
    const result = await this.groupsRepository.update(id, updateGroupDto);

    return result;
  }

  async deleteGroupById(id: number): Promise<DeleteResult> {
    const result = await this.groupsRepository.delete(id);

    return result;
  }
}
