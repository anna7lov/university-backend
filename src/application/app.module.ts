import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StudentsModule } from '../students/students.module';
import { typeOrmAsyncConfig } from 'src/configs/database/typeorm-config';
import { GroupsModule } from '../groups/groups.module';
import { CoursesModule } from '../courses/courses.module';
import { LectorsModule } from '../lectors/lectors.module';
import { MarksModule } from '../marks/marks.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    StudentsModule,
    GroupsModule,
    CoursesModule,
    LectorsModule,
    MarksModule,
    AuthModule,
  ],
})
export class AppModule {}
