import { ApiProperty } from '@nestjs/swagger';

export class ServerErrorResponseDto {
  @ApiProperty({ example: 'Internal Server Error' })
  message: string;

  @ApiProperty({ example: '500' })
  statusCode: number;
}
