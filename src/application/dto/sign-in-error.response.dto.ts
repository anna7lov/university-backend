import { ApiProperty } from '@nestjs/swagger';

export class SignInErrorResponseDto {
  @ApiProperty({ example: 'Invalid email or password' })
  message: string;

  @ApiProperty({ example: 'Unauthorized' })
  error: string;

  @ApiProperty({ example: '401' })
  statusCode: number;
}
