import { Mark } from '../entities/mark.entity';

export interface ICourseMark extends Pick<Mark, 'mark'> {
  courseName: string;
  lectorName: string;
  studentName: string;
}
