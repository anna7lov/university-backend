import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Mark } from './entities/mark.entity';
import { CoursesService } from 'src/courses/courses.service';
import { StudentsService } from 'src/students/students.service';
import { LectorsService } from 'src/lectors/lectors.service';
import { IStudentMark } from './interfaces/student-mark.interface';
import { ICourseMark } from './interfaces/course-mark.interface';
import { AddMarkDto } from './dto/add-mark.dto';
import { UpdateMarkDto } from './dto/update-mark.dto';

@Injectable()
export class MarksService {
  constructor(
    @InjectRepository(Mark)
    private readonly marksRepository: Repository<Mark>,
    private readonly studentsService: StudentsService,
    private readonly coursesService: CoursesService,
    private readonly lectorsService: LectorsService,
  ) {}

  async findMarkById(markId: number): Promise<Mark | null> {
    const mark = await this.marksRepository.findOne({
      where: { id: markId },
    });
    return mark;
  }

  async getStudentMarks(studentId: number): Promise<IStudentMark[]> {
    const student = await this.studentsService.findStudentById(studentId);

    if (!student) {
      throw new NotFoundException('Student not found');
    }

    const marks = await this.marksRepository
      .createQueryBuilder('mark')
      .select(['course.name AS "courseName"', 'mark.mark AS mark'])
      .leftJoin('mark.course', 'course')
      .where('mark.studentId = :studentId', { studentId })
      .getRawMany();

    return marks;
  }

  getCourseMarks = async (courseId: number): Promise<ICourseMark[]> => {
    const course = await this.coursesService.findCourseById(courseId);

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    const marks = await this.marksRepository
      .createQueryBuilder('mark')
      .select([
        'course.name AS "courseName"',
        'lector.name AS "lectorName"',
        'student.name AS "studentName"',
        'mark.mark AS mark',
      ])
      .leftJoin('mark.course', 'course')
      .leftJoin('mark.lector', 'lector')
      .leftJoin('mark.student', 'student')
      .where('course.id = :courseId', { courseId })
      .getRawMany();

    return marks;
  };

  async addMark(markAddDto: AddMarkDto): Promise<Mark> {
    const course = await this.coursesService.findCourseById(
      markAddDto.courseId,
    );
    const student = await this.studentsService.findStudentById(
      markAddDto.studentId,
    );
    const lector = await this.lectorsService.findLectorById(
      markAddDto.lectorId,
    );

    if (!course || !student || !lector) {
      throw new NotFoundException('Course, Student or Lector not found');
    }

    if (
      !(await this.coursesService.courseLectorRelationExists(
        markAddDto.courseId,
        markAddDto.lectorId,
      ))
    ) {
      throw new ForbiddenException(
        'Lector is not assigned to teach this course',
      );
    }
    return this.marksRepository.save(markAddDto);
  }

  async updateMarkById(
    id: number,
    updateMarkDto: UpdateMarkDto,
  ): Promise<UpdateResult> {
    const course = await this.coursesService.findCourseById(
      updateMarkDto.courseId,
    );
    const student = await this.studentsService.findStudentById(
      updateMarkDto.studentId,
    );
    const lector = await this.lectorsService.findLectorById(
      updateMarkDto.lectorId,
    );

    if (!course || !student || !lector) {
      throw new NotFoundException('Course, Student or Lector not found');
    }

    if (
      !(await this.coursesService.courseLectorRelationExists(
        updateMarkDto.courseId,
        updateMarkDto.lectorId,
      ))
    ) {
      throw new ForbiddenException(
        'Lector is not assigned to teach this course',
      );
    }

    const result = await this.marksRepository.update(id, updateMarkDto);

    return result;
  }

  async deleteMarkById(id: number): Promise<DeleteResult> {
    const result = await this.marksRepository.delete(id);

    return result;
  }
}
