import { ApiProperty } from '@nestjs/swagger';

export class GetStudentMarkResponseDto {
  @ApiProperty({ description: 'The course name' })
  courseName: string;

  @ApiProperty({
    minimum: 0,
    maximum: 10,
    description: 'The mark',
    example: '10',
  })
  mark: string;
}
