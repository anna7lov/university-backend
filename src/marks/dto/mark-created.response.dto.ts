import { ApiProperty } from '@nestjs/swagger';

export class MarkCreatedResponseDto {
  @ApiProperty({ description: 'The mark auto-generated id', example: '1' })
  id: number;

  @ApiProperty({ description: 'The date and time the mark was added' })
  createdAt: Date;

  @ApiProperty({ description: 'The date and time the mark was updated' })
  updatedAt: Date;

  @ApiProperty({
    minimum: 0,
    maximum: 10,
    description: 'The mark',
    example: '10',
  })
  mark: number;

  @ApiProperty({ description: 'The course id', example: '1' })
  courseId: number;

  @ApiProperty({ description: 'The student id', example: '1' })
  studentId: number;

  @ApiProperty({ description: 'The lector id', example: '1' })
  lectorId: number;
}
