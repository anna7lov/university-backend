import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { MarksController } from './marks.controller';
import { MarksService } from './marks.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';
import { CoursesModule } from 'src/courses/courses.module';
import { LectorsModule } from 'src/lectors/lectors.module';
import { StudentsModule } from 'src/students/students.module';
import { CheckMarkExistsMiddleware } from './middlewares/mark.middleware';

@Module({
  imports: [
    TypeOrmModule.forFeature([Mark]),
    StudentsModule,
    CoursesModule,
    LectorsModule,
  ],
  controllers: [MarksController],
  providers: [MarksService],
  exports: [MarksService],
})
export class MarksModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(CheckMarkExistsMiddleware)
      .forRoutes(
        { path: 'marks/:id', method: RequestMethod.PATCH },
        { path: 'marks/:id', method: RequestMethod.DELETE },
      );
  }
}
