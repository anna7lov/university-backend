import { Module } from '@nestjs/common';
import { ResetTokenService } from './reset-token.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResetToken } from './entities/reset-token.entity';
import { LectorsModule } from 'src/lectors/lectors.module';

@Module({
  imports: [TypeOrmModule.forFeature([ResetToken]), LectorsModule],
  providers: [ResetTokenService],
  exports: [ResetTokenService],
})
export class ResetTokenModule {}
