import { ApiProperty } from '@nestjs/swagger';
import { GetCourseResponseDto } from 'src/courses/dto/get-course.response.dto';

export class GetLectorWithCoursesResponseDto {
  @ApiProperty({ description: 'The lector auto-generated id', example: '1' })
  id: number;

  @ApiProperty({ description: 'The date and time the lector was created' })
  createdAt: Date;

  @ApiProperty({ description: 'The date and time the lector was updated' })
  updatedAt: Date;

  @ApiProperty({ description: 'The lector name' })
  name: string;

  @ApiProperty({ format: 'email', description: 'The lector email' })
  email: string;

  @ApiProperty({
    type: GetCourseResponseDto,
    isArray: true,
    description: 'The lector courses',
  })
  courses: GetCourseResponseDto[];
}
