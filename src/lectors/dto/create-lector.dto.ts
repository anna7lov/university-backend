import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class CreateLectorDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'The lector name' })
  name: string;

  @IsEmail()
  @ApiProperty({ format: 'email', description: 'The lector email' })
  email: string;

  @IsString()
  @MinLength(6)
  @ApiProperty({
    format: 'password',
    minLength: 6,
    description: 'The lector password',
  })
  password: string;
}
