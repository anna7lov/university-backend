import { ApiProperty } from '@nestjs/swagger';

export class LectorDto {
  @ApiProperty({ description: 'The lector auto-generated id', example: '1' })
  id: number;

  @ApiProperty({ description: 'The date and time the lector was created' })
  createdAt: Date;

  @ApiProperty({ description: 'The date and time the lector was updated' })
  updatedAt: Date;

  @ApiProperty({ description: 'The lector name' })
  name: string;

  @ApiProperty({ format: 'email', description: 'The lector email' })
  email: string;
}
