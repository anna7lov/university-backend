import {
  BadRequestException,
  Injectable,
  NestMiddleware,
  NotFoundException,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { LectorsService } from '../lectors.service';

@Injectable()
export class CheckLectorExistsMiddleware implements NestMiddleware {
  constructor(private readonly lectorsService: LectorsService) {}

  async use(request: Request, response: Response, next: NextFunction) {
    const { id } = request.params;

    if (isNaN(+id)) {
      throw new BadRequestException(
        'Validation failed (numeric string is expected)',
      );
    }

    const lector = await this.lectorsService.findLectorById(+id);

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    next();
  }
}
