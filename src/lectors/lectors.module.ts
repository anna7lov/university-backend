import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { LectorsController } from './lectors.controller';
import { LectorsService } from './lectors.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lector } from './entities/lector.entity';
import { CheckLectorExistsMiddleware } from './middlewares/lector.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([Lector])],
  controllers: [LectorsController],
  providers: [LectorsService],
  exports: [LectorsService],
})
export class LectorsModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(CheckLectorExistsMiddleware)
      .forRoutes(
        { path: 'lectors/:id', method: RequestMethod.PATCH },
        { path: 'lectors/:id', method: RequestMethod.DELETE },
      );
  }
}
