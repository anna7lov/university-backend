import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { LectorsService } from './lectors.service';
import { GetLectorWithCoursesResponseDto } from './dto/get-lector-with-courses.response.dto';
import { LectorDto } from './dto/lector.dto';
import { CreateLectorDto } from './dto/create-lector.dto';
import { ServerErrorResponseDto } from 'src/application/dto/server-error.response.dto';
import { IdValidationErrorResponseDto } from 'src/application/dto/id-validation-error.respose.dto';
import { ValidationErrorResponseDto } from 'src/application/dto/validation-error.response.dto';
import { NotFoundResponseDto } from 'src/application/dto/not-found.response.dto';
import { Lector } from './entities/lector.entity';
import { ILectorWithCourses } from './interfaces/lector-with-courses.interface';
import { AuthGuard } from 'src/auth/guards/auth.guard';
import { UpdateLectorDto } from './dto/update-lector.dto';
import { DeleteResult, UpdateResult } from 'typeorm';
import { AuthErrorResponseDto } from 'src/application/dto/auth-error.response.dto';
import { CurrentUser } from 'src/auth/decorators/current.user.decorator';

@UseGuards(AuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiTags('Lectors')
@Controller('lectors')
export class LectorsController {
  constructor(private readonly lectorsService: LectorsService) {}

  @ApiOperation({ summary: 'Get user' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: LectorDto,
    description: 'The user',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'User was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Get('me')
  public async findMe(@CurrentUser() loggedUser: any) {
    return await this.lectorsService.findCurrentUser(loggedUser.userId);
  }

  @ApiOperation({ summary: 'Get all lectors' })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: LectorDto,
    description: 'The list of lectors',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Get()
  getAllLectors(): Promise<Lector[]> {
    return this.lectorsService.getAllLectors();
  }

  @ApiOperation({ summary: 'Get lector with courses by id' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: GetLectorWithCoursesResponseDto,
    description: 'The lector with courses by id',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: IdValidationErrorResponseDto,
    description: 'Id validation  failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The lector was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Get(':id')
  getLectorWithCoursesById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<ILectorWithCourses> {
    return this.lectorsService.getLectorWithCoursesById(id);
  }

  @ApiOperation({ summary: 'Create lector' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: LectorDto,
    description: 'The lector was created',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Post()
  createLector(@Body() createLectorDto: CreateLectorDto): Promise<Lector> {
    return this.lectorsService.createLector(createLectorDto);
  }

  @ApiOperation({
    summary: 'Update lector by id',
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The lector was updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The lector was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Patch(':id')
  updateLectortById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateLectorDto: UpdateLectorDto,
  ): Promise<UpdateResult> {
    return this.lectorsService.updateLectorById(id, updateLectorDto);
  }

  @ApiOperation({ summary: 'Delete course by id' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The course was deleted',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: IdValidationErrorResponseDto,
    description: 'Id validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The course was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  deleteLectorById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<DeleteResult> {
    return this.lectorsService.deleteLectorById(id);
  }
}
