import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsString,
  IsNumber,
  IsOptional,
  IsEmail,
  IsNotEmpty,
  Max,
  Min,
} from 'class-validator';

export class CreateStudentDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'The student name' })
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'The student surname' })
  surname: string;

  @IsEmail()
  @ApiProperty({
    format: 'email',
    description: 'The student email',
  })
  email: string;

  @IsNumber()
  @Min(14)
  @Max(99)
  @ApiProperty({ description: 'The student age', example: '20' })
  age: number;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'The path to student image' })
  imagePath: string;

  @IsNumber()
  @Min(1)
  @IsOptional()
  @ApiPropertyOptional({ description: 'The student group id', example: '1' })
  groupId: number | null;
}
