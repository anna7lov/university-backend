import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Group } from 'src/groups/entities/group.entity';
import { Mark } from 'src/marks/entities/mark.entity';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @Index()
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  surname: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @Column({
    type: 'numeric',
    nullable: true,
  })
  age: number;

  @Column({
    name: 'image_path',
    type: 'varchar',
    nullable: false,
  })
  imagePath: string;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: true,
    eager: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'group_id' })
  group: Group | null;

  @Column({
    type: 'integer',
    nullable: true,
    name: 'group_id',
  })
  groupId: number | null;

  @OneToMany(() => Mark, (mark) => mark.student)
  marks: Mark[];
}
