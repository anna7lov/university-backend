import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Student } from './entities/student.entity';
import { GroupsService } from 'src/groups/groups.service';
import { SearchByNameDto } from './dto/search-by-name.dto';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentsRepository: Repository<Student>,
    private readonly groupsService: GroupsService,
  ) {}

  async findStudentById(studentId: number): Promise<Student | null> {
    const student = await this.studentsRepository.findOne({
      where: { id: studentId },
    });
    return student;
  }

  async getAllStudents(name: SearchByNameDto): Promise<Student[]> {
    const queryBuilder = this.studentsRepository.createQueryBuilder('student');

    if (name) {
      queryBuilder.where('student.name = :name', { name });
    }

    const students = await queryBuilder.getMany();

    return students;
  }

  async getStudentById(id: number): Promise<Student> {
    const student = await this.studentsRepository.findOne({ where: { id } });

    if (!student) {
      throw new NotFoundException('Student not found');
    }

    return student;
  }

  async createStudent(createStudentDto: CreateStudentDto): Promise<Student> {
    const student = await this.studentsRepository.findOne({
      where: { email: createStudentDto.email },
    });

    if (student) {
      throw new BadRequestException('Student with this email already exists');
    }

    if (createStudentDto.groupId) {
      const group = await this.groupsService.findGroupById(
        createStudentDto.groupId,
      );

      if (!group) {
        throw new NotFoundException('Group with this id was not found');
      }
    }

    return this.studentsRepository.save(createStudentDto);
  }

  async updateStudentById(
    id: number,
    updateStudentDto: UpdateStudentDto,
  ): Promise<UpdateResult> {
    if (updateStudentDto.groupId) {
      const group = await this.groupsService.findGroupById(
        updateStudentDto.groupId,
      );

      if (!group) {
        throw new NotFoundException('Group with this id was not found');
      }
    }

    const result = await this.studentsRepository.update(id, updateStudentDto);

    return result;
  }

  async deleteStudentById(id: number): Promise<DeleteResult> {
    const result = await this.studentsRepository.delete(id);

    return result;
  }
}
