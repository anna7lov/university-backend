import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddResetTokenTable1693560540937 implements MigrationInterface {
  name = 'AddResetTokenTable1693560540937';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "reset_tokens" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "token" character varying NOT NULL, "lector_id" integer NOT NULL, CONSTRAINT "REL_c3d46e6cc811d3ac7df4bfb9c8" UNIQUE ("lector_id"), CONSTRAINT "PK_acd6ec48b54150b1736d0b454b9" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "reset_tokens" ADD CONSTRAINT "FK_c3d46e6cc811d3ac7df4bfb9c86" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "reset_tokens" DROP CONSTRAINT "FK_c3d46e6cc811d3ac7df4bfb9c86"`,
    );
    await queryRunner.query(`DROP TABLE "reset_tokens"`);
  }
}
